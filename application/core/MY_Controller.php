<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('curl');			
		$headers = $this->input->request_headers();
		
		$token = 'my_token';
		
		if(!isset($headers['Token']) || strcmp($headers['Token'],$token) != 0) {
			
			response(401,'Unauthorized');
			die;	
		}
		
 	}
}
?>