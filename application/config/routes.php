<?php defined('BASEPATH') OR exit('No direct script access allowed');
$route['404_override']         = '';
$route['translate_uri_dashes'] = TRUE;
$route['default_controller']   = '';

$route['execute/(:num)'] = 'execute/index/$1';
$route['comment/(:num)'] = 'comment/index/$1';
