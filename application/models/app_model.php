<?php
class App_model extends MY_model{
	function __Construct(){
		parent::__Construct();
		$this->load->library(array('pagination','curl'));

	}
	public function block_chart($user_id)
	{
		$data['total_posts'] = $this->db->where(array('user_id'=>$user_id))->get('ap_posts')->num_rows();
		$data['total_follow_pages'] = $this->db->where(array('user_id'=>$user_id))->get('ap_follow')->num_rows();
		$data['total_pages'] 	  = $this->db->where('user_id',$user_id)->get('pages')->num_rows();
		$data['total_scheduling'] = $this->db->where(array('user_id'=>$user_id,'status'=>0))->get('ap_posts')->num_rows();
		return $data;
	}
	public function pages_dashboard($user_id)
	{
		$data['pages'] = $this->db->select('pages.id,pages.page_fbid,pages.page_token,page_name,page_picture,page_like,page_error,ap_pages.page_time_run')
			->where('user_id',$this->user_id)
			->join('ap_pages','pages.id = ap_pages.page_id','left')
			->get('pages')->result_array();
			return $data;
	}
	
}
?>