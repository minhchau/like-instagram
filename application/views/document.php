<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Untitled Document</title>

	<meta name="robots" content="noindex">
	<meta name="googlebot" content="noindex">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<style>

	</style>

</head>

<body>

<div class="container">
	<h2 class="text-center">Documents</h2>

	<div>
		<h4>I. Follow</h4>
		<div class="container" style="border:1px solid #ddd;border-radius:4px">
			<div style="padding:15px;">
				<span style="color:#999;font-size:17px">/api/getList/</span>
			</div>
			<table class="table">
				<thead>
				<tr>
					<th>Name</th>
					<th>Type</th>
					<th>Description</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td>acc_id</td>
					<td>number</td>
					<td>Lấy thông tin ID acount</td>
				</tr>
			</table>
		</div>
	</div>
	</hr>

	<div>
		<h4>II. Insert data follow</h4>
		<div class="container" style="border:1px solid #ddd;border-radius:4px">
			<div style="padding:15px;">
				<span style="color:#999;font-size:17px">/api/getData</span>
			</div>
			<table class="table">
				<thead>
				<tr>
					<th>Name</th>
					<th>Type</th>
					<th>Description</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td>acc_id</td>
					<td>number</td>
					<td>Lấy thông tin tài khoản cho tài khoản instagram:</td>
				</tr>
				<tr>
					<td>people_follow</td>
					<td>json</td>
					<td>lấy thông tin follow</td>
				</tr>
				<tr>
					<td>follow_dellay</td>
					<td>json</td>
					<td>Thời gian giữa 2 lần follow</td>
				</tr>
				<tr>
					<td>operation_delay</td>
					<td>json</td>
					<td>Thời gian giữa 2 lượt follow</td>
				</tr>
				<tr>
					<td>follow_max_day</td>
					<td>json</td>
					<td>Số lượng follow trong 1 ngày nhiều nhất.</td>
				</tr>
				<tr>
					<td>excute_time</td>
					<td>json</td>
					<td>Follow trong khoảng thời gian</td>
				</tr>
				<tr>
					<td>user_image_profile</td>
					<td>number (0,1)</td>
					<td>Lấy thông tin user instagram có ảnh đại diện</td>
				</tr>
				<tr>
					<td>user_private</td>
					<td>number (0,1)</td>
					<td>Thông tin user instagram có public hay không</td>
				</tr>
				<tr>
					<td>source_follow</td>
					<td>json</td>
					<td>Thông tin user cần follow </td>
				</tr>
			</table>
		</div>
	</div>

<!--	<div>-->
<!--		<h4>II. Insert data source follow</h4>-->
<!--		<div class="container" style="border:1px solid #ddd;border-radius:4px">-->
<!--			<div style="padding:15px;">-->
<!--				<span style="color:#999;font-size:17px">/api/sourceFollow</span>-->
<!--			</div>-->
<!--			<table class="table">-->
<!--				<thead>-->
<!--				<tr>-->
<!--					<th>Name</th>-->
<!--					<th>Type</th>-->
<!--					<th>Description</th>-->
<!--				</tr>-->
<!--				</thead>-->
<!--				<tbody>-->
<!--				<tr>-->
<!--					<td>action</td>-->
<!--					<td>number(1,2)</td>-->
<!--					<td>1: Thông tin keyword, 2: Thông tin user instagram</td>-->
<!--				</tr>-->
<!--				<tr>-->
<!--					<td>acc_action</td>-->
<!--					<td>number</td>-->
<!--					<td>Lấy thông tin ID ac_action</td>-->
<!--				</tr>-->
<!--				<tr>-->
<!--					<td>keyword</td>-->
<!--					<td>string</td>-->
<!--					<td>Lây thông tin keyword</td>-->
<!--				</tr>-->
<!--			</table>-->
<!--		</div>-->
<!--	</div>-->


</div>

</body>
</html>