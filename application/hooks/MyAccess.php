<?php
class Access{
    public $CI;
    public function __construct(){
        $this->CI =& get_instance();
		$this->CI->load->library('Zend');
		    }
    public function start(){
        $this->CI->zend->load('Zend_Acl');
        $module     = $this->CI->router->fetch_module();
        $controller = $this->CI->router->fetch_class();
        $action     = $this->CI->router->fetch_method();
		$session    = $this->CI->session->all_userdata();
		$role_id    = 3;
		$role       = 'guest';
		$redirect   = 'notlogin';
		$user_id    = '';
		if(!empty($session['user_data'])){
			$role_id = $session['user_data']['role_id'];	
			$role    = $session['user_data']['role_name'];
			$user_id    = $session['user_data']['id'];
			if($session['user_data']['notpermission_to'])
				$redirect =	$session['user_data']['notpermission_to'];
		}
		$this->setResources();
        $this->setRoles($role_id,$user_id,$role);
		if(!$this->CI->Zend_Acl->isAllowed($role,$module.':'.$controller, $action)){
			redirect(base_url().$redirect);
		}
		else{
			if($role != 'guest'){
				$accept = true;
				if(empty($session['fb_config']['app_id'])){//Chưa có app
					$this->CI->session->set_flashdata('app_warning','Hiện tại bạn không có App nào <button class="btn btn-default btn-sm" onclick="showbox(\'box-add-app\')">Click here</button> để thêm một App mới');
					$accept = false;
				}
				else if($session['fb_config']['active'] == 0){//Có app nhưng chưa kích hoạt
					$this->CI->session->set_flashdata('app_warning','Please active an App to use');	
					$accept = false;
				}
				else if($session['fb_config']['error'] == 1){//Có app đã kích hoạt nhưng bị lỗi
					$this->CI->session->set_flashdata('app_warning','App của bạn bị lỗi access token! <button class="btn btn-default btn-sm" onclick="getAccesstoken('.$session['fb_config']['id'].')">Click here</button> để sửa lại');	
					$accept = false;
				}
				if($accept == false){
					if($module != 'primary' || $action == 'page_manager'){
						redirect(base_url().'setting/app-manager');	
					}	
				}
			}
		}
    }
	public function setResources(){
		$aclresources = $this->CI->db->where_in('type',array('module','controller'))->get('aclresources');
		foreach($aclresources->result() as $row){
			if($row->type == 'module'){
				$this->CI->Zend_Acl->add(new Zend_Acl_Resource($row->module));	
			}
			else{
				$this->CI->Zend_Acl->add(new Zend_Acl_Resource($row->module.':'.$row->controller),$row->module);
			}
		}
    }
    public function setRoles($role_id,$user_id,$role){
		$this->CI->Zend_Acl->addRole(new Zend_Acl_Role($role));
		if($role == 'admin'){
			$this->CI->Zend_Acl->allow($role);
		}
		else{
			$where_in = array($role_id);
			if($user_id)
				$where_in[] = $user_id;
			$this->CI->db->from('acl');
			$this->CI->db->join('aclresources','acl.resource_id = aclresources.id');
			$acl_role = $this->CI->db->where_in('role_id',$where_in)->where(array('type'=>'action'))->get();
			$allow = array();
			foreach($acl_role->result() as $row){
				//if(!in_array($row->action,$allow[$row->module.':'.$row->controller]))
				$allow[$row->module.':'.$row->controller][] = $row->action;
			}
			foreach($allow as $resource => $actions){
				$this->CI->Zend_Acl->allow($role,$resource,$actions);	
			}
			$this->CI->Zend_Acl->deny($role,'invite:invite_home');
		}
    }
}