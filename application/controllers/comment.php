<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class comment extends CI_Controller
{
    public $acc_id;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
        date_default_timezone_set("America/Los_Angeles");
    }

    public function index($id)
    {
        $max_time_exec = time() + 150;//Tối đa 150 giây

        if (!isset($id)) {
            exit;
        }

        $acc = $this->input->post();

        if (!$acc) {
            $this->acc_id = $id;

            $param = array(
                'acc_id' => $this->acc_id,
                'proxy' => true,
            );

            $res = $this->curl->instadaily('account/get', true, $param);

            $res = json_decode($res, true);
            $acc = current($res['data']);
            $cookies = json_decode($acc['cookies'], true);
        } else {
            if (!$acc) {
                exit;
            }

            $this->acc_id = $id;

            $cookies = json_decode($acc['cookies'], true);
            $setup = json_decode($acc['setting'], true);
            $proxy = $acc['proxy'];
            $proxy_auth = $acc['proxy_auth'];
            $aca_id = $acc['aca_id'];
        }

        // ---------------------begin------------------------
//        $cookies = array(
//            'csrftoken' => 'tBXaZ5k0XKkVwwJBAVngkSQD37GjVh7K',
//            'rur' => 'ATN',
//            'mid' => 'WyDy9AABAAGBefnit24rO7LBbcZX',
//            'mcd' => 3,
//            'shbid' => 359,
//            'shbts' => 1529918207.2013276,
//            'ds_user_id' => 179189981,
//            'sessionid' => 'IGSC23735efa55cfd7416899f0d9a04855480ae8fd13ad4498ccfc6ecbf58aef7f35:WkjOZdEt8MqkXLIQo0ldqnRrpQ1vor9g:{"_auth_user_id":179189981,"_auth_user_backend":"accounts.backends.CaseInsensitiveModelBackend","_auth_user_hash":"","_platform":4,"_token_ver":2,"_token":"179189981:MqwroFvXk8aF0vXOvnm6BmI4Vix4YDRj:4dd327612aa1e4fda7ccf9ad74df083fa2241059ee38c1ad1324f14de154c1ec","last_refreshed":1529911394.1022665501}',
//            'urlgen' => '{\"time\": 1529917019}:1fXNc7:T90VxMNbg2sRLtrRJ2i_uwPoAfM'
//        );
        $proxy_auth = '';

        $proxy = '';
        $acc['setting'] = '{"like_ope":["8","15"],"like_day":["250","300"],"delay_like":["15","20"],"delay_ope":["45","60"],"execute_time":["11","16"],"post_day":0,"like_post":["0", "100"], "comment_post":["0","100"]}';
        $setup = json_decode($acc['setting'], true);
        $acc['results'] = 1;

        //-----------------end---------------------------

        $likePost = $setup['like_post'];
        $commentPost = $setup['comment_post'];
        $postDay = $setup['post_day'];


        $setting = $this->db->select('max_day, max_ope, result_day, result_ope')->where('acc_id', $this->acc_id)->get('settings')->row_array();

        $source = $this->get_source($cookies, $proxy, $proxy_auth, $setting['max_ope']);


        if ($source == false) {
            $update_action = array(
                'error' => 1
            );
            $this->curl->instadaily('action/update/' . $acc['aca_id'], true, $update_action);
            exit;
        }

        sleep(1);

        $update_action = array(
            'results' => $acc['results']
        );

        foreach ($source['users'] as $row) {
            if (time() >= $max_time_exec) {//Nếu vượt quá thời gian thực thi script là 110 giây thì thoát
                break;
            }
            $row = current($row);

            if ($this->check_day($row, $postDay) == false) {
                continue;
            }

            $exec = $this->exec_comment($row['id'], $cookies, $proxy, $proxy_auth);

            if ($exec !== false) {
                //Cập nhập lại cookie mới
                foreach ($exec['cookies'] as $key => $cookie) {
                    $cookies[$key] = $cookie;
                }

                $update_action['results']++;

                $setting['result_day']++;
                $setting['result_ope']++;


                if ($setting['max_day'] <= $setting['result_day']) {//Đạt giới hạn follow trong ngày

                    $setting['max_ope'] = rand($setup['like_ope'][0], $setup['like_ope'][1]);
                    $setting['max_day'] = rand($setup['like_day'][0], $setup['like_day'][1]);
                    $setting['result_day'] = 0;
                    $setting['result_ope'] = 0;

                    $update_action['time_run'] = $this->get_time($setup['delay_ope'], $setup['execute_time'], 2);

                    break;
                }

                if ($setting['max_ope'] <= $setting['result_ope']) {//Đạt giới hạn follow trong 1 lượt

                    $setting['max_ope'] = rand($setup['like_ope'][0], $setup['like_ope'][1]);
                    $setting['result_ope'] = 0;

                    $get_time = $this->get_time($setup['delay_ope'], $setup['execute_time'], 1);

                    if ($get_time['new_day'] == true) {
                        $setting['max_day'] = rand($setup['like_day'][0], $setup['like_day'][1]);
                        $setting['result_day'] = 0;
                    }

                    $update_action['time_run'] = $get_time['nexttime'];
                    break;
                }

            } else {
                $update_action['error'] = 1;
                break;
            }

            $sleep = $setup['delay_like'];
            $sleep = rand($sleep[0], $sleep[1]);
            sleep($sleep);
        }
        die;


        // Nếu trả lời hết 12 người thì mới update next_page cho bảng source_users

        $update_source['next_page'] = $source['next_page'];
        if ($source['end']) {
            $update_source['status'] = 2;
        }
        $this->db->where('id', $source['id'])->set($update_source)->update('source_users');

        //Update bảng setting

        $this->db->where('acc_id', $this->acc_id)->set($setting)->update('settings');

        //Update account
        $update_account = array(
            'cookies' => json_encode($cookies),
            'token' => $cookies['csrftoken']
        );

        $this->curl->instadaily('account/update/' . $this->acc_id, true, $update_account);
        //update account action
//        $this->curl->instadaily('action/update/' . $acc['aca_id'], true, $update_action);

    }


    private function get_source($cookies, $proxy, $proxy_auth, $limit)
    {
        $source = $this->db->where(array('acc_id' => $this->acc_id, 'status' => 1))->order_by('id', 'asc')->limit(1)->get('source_users')->row_array();

        $insta_id = $source['pk'];

        // lấy ra tổng posts của insta_id
        $info = $this->getInfo($insta_id);

        $media_count = $info['media_count'];

        if (!$source['next_page']) {
            $param = array(
                'query_hash' => '6305d415e36c0a5f0abb6daba312f2dd',
                'variables' => '{"id":"' . $insta_id . '","first":"'.$limit.'","after":""}'
            );
        } elseif ($source['next_page'] && $media_count > $source['media_count']) {
            $number = $media_count - $source['media_count'];
            $param = array(
                'query_hash' => '6305d415e36c0a5f0abb6daba312f2dd',
                'variables' => '{"id":"' . $insta_id . '","first":"' . $number . '" ,"after":""}'
            );
        } else {
            $param = array(
                'query_hash' => '6305d415e36c0a5f0abb6daba312f2dd',
                'variables' => '{"id":"' . $insta_id . '","first":'.$limit.',"after":"' . $source['next_page'] . '"}'
            );
        }

        $header = array(
            'x-csrftoken: ' . $cookies['csrftoken'],
            'cookie: ' . $this->cookie_string($cookies),
        );

        $param = http_build_query($param);

        $res = $this->curl->instagram('graphql/query/?' . $param, false, false, $header, '', $proxy_auth);

        $response = json_decode($res['response'], true);

        if ($res['code'] != 200 || json_last_error() !== JSON_ERROR_NONE || !isset($response['status']) || $response['status'] != 'ok') {
            $log = array(
                'acc_id' => $this->acc_id,
                'type' => 'source',
                'code' => $res['code'],
                'header' => '',
                'response' => $res['response'],
                'created' => time()
            );
            $this->db->insert('logs', $log);
            return false;
        }

        $users = $response['data']['user']['edge_owner_to_timeline_media']['edges'];
        $next_page = $response['data']['user']['edge_owner_to_timeline_media']['page_info']['end_cursor'];
        $has_page = $response['data']['user']['edge_owner_to_timeline_media']['page_info']['has_next_page'];

        // cập nhập media_count
        if ($media_count > $source['media_count']) {
            $this->db->where('id', $source['id'])->set(array('media_count' => $media_count))->update('source_users');
        }

        if ($media_count > $source['media_count'] && $source['next_page'] != '') {
            $next_page = $source['next_page'];
        }

        return array(
            'id' => $source['id'],
            'users' => $users,
            'next_page' => $next_page,
            'end' => $has_page ? 0 : 1,
        );
    }

    private function exec_comment($id, $cookies, $proxy)
    {

        $url = 'web/comments/'.$id.'/add/';
        $message = 'hi';
        $data = ([
            'comment_text' => $message
        ]);
        $header = array(
            'content-type: application/x-www-form-urlencoded',
            'x-csrftoken: ' . $cookies['csrftoken'],
            'cookie: ' . $this->cookie_string($cookies),
            'origin: https://www.instagram.com',
            'referer: https://www.instagram.com/'
        );
        $res = $this->curl->instagram($url, true, $data, $header,  '', true, '');

        $response = json_decode($res['response'],true);

        //Nếu bị lỗi thì return lỗi và kết quả báo lỗi
        if($res['code'] != 200 || json_last_error() !== JSON_ERROR_NONE || !isset($response['status']) || $response['status'] != 'ok') {
            $log = array(
                'acc_id'   => $this->acc_id,
                'type'     => 'exec',
                'code'     => $res['code'],
                'header'   => $res['header'],
                'response' => $res['response'],
                'created'  => time()
            );

            $this->db->insert('logs',$log);

            return false;
        }
        //không lỗi thì trả về success true
        return array(
            'success' => true,
            'cookies' => $this->get_cookie($res['header'])
        );

    }
    private function check_avatar($url)
    {
        if (preg_match('/\/5BBBE67A\//', $url))
            return false;
        else
            return true;
    }

    /*
     * check like posts
     */

    private function check_like($data, $postLike)
    {
        $like = $data['edge_media_preview_like']['count'];
        if ($postLike['0'] == $postLike['0']) {
            return true;
        }
        if ($postLike['0'] < $like && $like < $postLike['1']) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * check comment Posts
     */

    private function check_comment($data, $postComment)
    {
        $comment = $data['edge_media_to_comment']['count'];
        if ($postComment['0'] == $postComment['0']) {
            return true;
        }
        if ($postComment['0'] < $comment && $comment < $postComment['1']) {
            return true;
        } else {
            return false;
        }
    }


    /*
     *  check user instagram private
     */

    private function check_private($id)
    {
        $user = $this->getInfo($id);

        if ($user && $user['is_private'] == true) {
            return true;
        }

        return false;
    }

    /*
     * check posts day
     */

    private function check_day($data, $post_day)
    {
        if ($post_day == 0) {
            return true;
        }
        if ($post_day > 0) {
            $time = $data['taken_at_timestamp'];
            if (time() - $post_day * 86400 < 0) {
                return true;
            }
        }
        return false;
    }


    private function get_time($delay, $exec_time, $type)
    {
        $timetoday = strtotime('TODAY');

        $start = $timetoday + $exec_time[0] * 3600;

        $end = $timetoday + $exec_time[1] * 3600;

        if ($end < $start) {
            if (intval(date('H')) < $exec_time[0]) {
                $start -= 86400;
            } else {
                $end += 86400;
            }

        }

        if ($type == 1) {

            $delay = rand($delay[0], $delay[1]) * 60;

            $nexttime = $delay + time();

            $new_day = false;

            if ($nexttime >= $end) {
                $nexttime = $start + 86400;
                $new_day = true;
            }

            return array('nexttime' => $nexttime, 'new_day' => $new_day);
        } else {
            $nexttime = $start + 86400;
            return $nexttime;
        }

    }

    // thông tin user instagram
    public function getInfo($insta_id)
    {

        $url = 'https://i.instagram.com/api/v1/users/' . $insta_id . '/info/';
        $res = $this->curl->call($url);
        $res = json_decode($res['response'], true);


        if (isset($res['status']) && $res['status'] == 'ok') {
            return $res['user'];
        }

        return array();
    }

    private function get_cookie($header)
    {
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);

        $cookies = array();

        foreach ($matches[1] as $item) {
            parse_str($item, $cookie);
            if (!trim(str_replace('"', '', current($cookie)))) {
                continue;
            }
            $cookies = array_merge($cookies, $cookie);
        }

        return $cookies;
    }

    private function cookie_string($cookies)
    {
        $cookie_string = '';
        foreach ($cookies as $key => $cookie) {

            $cookie_string .= $key . '=' . $cookie . ';';
        }
        return $cookie_string;
    }

    // comment instagram
    public function comment($id, $cookies, $proxy)
    {
        echo "1";
        die;
        $id = '1808024951531640217';
        $url = 'web/comments/' . $id . '/add/';
        $message = 'hi';
        $data = ([
            'comment_text' => $message
        ]);
        $header = array(
            'content-type: application/x-www-form-urlencoded',
            'x-csrftoken: ' . $cookies['csrftoken'],
            'cookie: ' . $this->cookie_string($cookies),
            'origin: https://www.instagram.com',
            'referer: https://www.instagram.com/'
        );
        $res = $this->curl->instagram($url, true, $data, $header, '', true, '');
        print_r($res);
        die;
    }
}

?>