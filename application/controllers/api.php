<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
    }

    public function get($acc_id)
    {
        $setting = $this->db->where('acc_id', $acc_id)->get('settings')->row_array();
        $return = array(
            'setting' => array(),

        );

        echo response(200, $return);

    }

    public function add($acc_id)
    {
        if(!isset($acc_id)) {
            exit;
        }

        $check = $this->db->where('acc_id', $acc_id)->get('settings')->num_rows();

        if($check > 0) {
            exit;
        }

        $setting = $this->input->post('setting');

        $this->db->insert('settings', $setting);


        echo response(200, 'insert');
    }

    public function update($acc_id)
    {
        if(!isset($acc_id))
        {
            exit;
        }

        $setting = $this->input->post('setting');

        if(!$setting){
            echo response(400, 'Fields invalid');
            exit;
        }

        $setting = json_decode($setting,true);

        $this->db->where('acc_id',$acc_id)->set($setting)->update('settings');

        echo response(200, 'update');
    }
}
?>