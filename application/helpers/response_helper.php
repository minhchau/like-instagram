<?php
function response($code, $message, $json=true) {
	if( !$code || !$message) {
		return '';	
	}
	
	if(is_array($message)) {
		$res = $message;
		$res['code'] = $code;
	}
	else{
		$res = array(
			'code' => $code,
			'message' => $message
		);
	}
	
	if($json == true) {
		$res = json_encode($res);	
	}
	
	//set_status_header($code);
	
	return $res;
}